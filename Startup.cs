using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authentication.OpenIdConnect;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.IdentityModel.Logging;

namespace GameFlix
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<CookiePolicyOptions>(options =>
            {
                // This lambda determines whether user consent for non-essential cookies is needed for a given request.
                options.CheckConsentNeeded = context => true;
                options.MinimumSameSitePolicy = SameSiteMode.None;
            });

            // Add authentication services
            services.AddAuthentication(options =>
            {
                options.DefaultAuthenticateScheme = CookieAuthenticationDefaults.AuthenticationScheme;
                options.DefaultSignInScheme = CookieAuthenticationDefaults.AuthenticationScheme;
                options.DefaultChallengeScheme = CookieAuthenticationDefaults.AuthenticationScheme;
                
            })
              .AddCookie()
              .AddOpenIdConnect("Auth0", options =>
              {
            // Set the authority to your Auth0 domain
            options.Authority = "https://gameflix.eu.auth0.com";

            // Configure the Auth0 Client ID and Client Secret
            options.ClientId = "QpgGpeI3ZbgebUdemcf9xGrY5MbXxJti";
            options.ClientSecret = "Gu08u-FbGC1jiCxaLRBXldfDQO2zD1Rq5nZ-ojKfPak9I8x-6g7Y3kPz-zdesWR3";
            options.SaveTokens = true;
                  // Set response type to code
                  options.ResponseType = "code";

                  options.RequireHttpsMetadata = false;

            // Configure the scope
            options.Scope.Clear();
                  options.Scope.Add("openid");

            // Set the callback path, so Auth0 will call back to http://localhost:3000/callback
            // Also ensure that you have added the URL as an Allowed Callback URL in your Auth0 dashboard
            options.CallbackPath = new PathString("/callback");

            // Configure the Claims Issuer to be Auth0
            options.ClaimsIssuer = "Auth0";
                 

                  options.Events = new OpenIdConnectEvents
                  {
                // handle the logout redirection
                OnRedirectToIdentityProviderForSignOut = (context) =>
                      {
                          var logoutUri =
                      $"https://gameflix.eu.auth0.com/v2/logout?client_id=QpgGpeI3ZbgebUdemcf9xGrY5MbXxJti";

                          var postLogoutUri = context.Properties.RedirectUri;
                          if (!string.IsNullOrEmpty(postLogoutUri))
                          {
                              if (postLogoutUri.StartsWith("/"))
                              {
                            // transform to absolute
                            var request = context.Request;
                                  postLogoutUri = request.Scheme + "://" + request.Host + request.PathBase + postLogoutUri;
                              }

                              logoutUri += $"&returnTo={Uri.EscapeDataString(postLogoutUri)}";
                          }

                          context.Response.Redirect(logoutUri);
                          context.HandleResponse();

                          return Task.CompletedTask;
                      },
                      OnRedirectToIdentityProvider = context =>
                      {
                          context.ProtocolMessage.SetParameter("audience", "http://localhost:5000/api");

                          return Task.FromResult(0);
                      }
                  };
              });

            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_3_0);
            services.AddControllersWithViews();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {


            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                IdentityModelEventSource.ShowPII = true;
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }
            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}
