﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using GameFlix.Models;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Hosting.Internal;
using Newtonsoft.Json;
using RestSharp;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace GameFlix.Controllers
{
    public class GameController : Controller
    {
        public IHostingEnvironment _hostingEnvironment;
        public GameController(IHostingEnvironment hostingEnvironment)
        {
            _hostingEnvironment = hostingEnvironment;
        }

        public async Task<IActionResult> Subscription()
        {/*
            string accessToken = await HttpContext.GetTokenAsync("access_token");
            var client = new RestClient("http://localhost:5000/api/games/3");
            var request = new RestRequest(Method.GET);
            request.AddHeader("authorization", "Bearer " + accessToken);
            IRestResponse response = client.Execute(request);*/

            var model = new UpgradeToPremiumViewModel();
            model.Duration = "30_days";

            return View(model);
        }

        public async Task<IActionResult> Show(int id)
        {
            var model = new Dictionary<string, string>();
            string accessToken = await HttpContext.GetTokenAsync("access_token");
            model.Add("access_token", accessToken);
            model.Add("id", id.ToString());
            ViewData["account_type"] = await Tools.GetAccountTypeAsync(accessToken);
            return View(model);
        }
        public IActionResult Create()
        {
            var model = new GameViewModel();
            model.Type = "demo";
            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> Create (GameViewModel obItem)
        {
            string accessToken = await HttpContext.GetTokenAsync("access_token");
            var uploads = Path.Combine(_hostingEnvironment.WebRootPath, "imgs");
            FileUploadHelper objFile = new FileUploadHelper();
            string strFilePath = await objFile.SaveFileAsync(obItem.formFile, uploads);
            strFilePath = strFilePath
             .Replace(_hostingEnvironment.WebRootPath, string.Empty)
             .Replace("\\", "/"); //Relative Path can be stored in database or do logically what is needed.

            var client = new RestClient("http://localhost:5000/api/Games/");
            var request = new RestRequest(Method.POST);
            //request.AddHeader("content-type", "application/json");
            request.AddHeader("authorization", "Bearer " + accessToken);
            request.AddJsonBody(new { Name = obItem.Name, 
                Publisher = obItem.Publisher, 
                Demo = obItem.Type == "demo", Price = obItem.Price, 
                PhotoName = strFilePath.Split('/').Last()});
            IRestResponse response = client.Execute(request);

           

            return this.RedirectToAction("Index", "Home");
        }
        [HttpGet]
        public async Task<IActionResult> Delete(int id)
        {
            string accessToken = await HttpContext.GetTokenAsync("access_token");
            var client = new RestClient("http://localhost:5000/api/Games/" + id);
            var request = new RestRequest(Method.DELETE);
            request.AddHeader("authorization", "Bearer " + accessToken);
            IRestResponse response = client.Execute(request);
            return this.RedirectToAction("Index", "Home");
        }
        [HttpGet]
        public async Task<IActionResult> Edit(int id)
        {
            string accessToken = await HttpContext.GetTokenAsync("access_token");
            var client = new RestClient("http://localhost:5000/api/Games/" + id);
            var request = new RestRequest(Method.GET);
            request.AddHeader("authorization", "Bearer " + accessToken);
            var response = client.Execute(request);
            var obj = JsonConvert.DeserializeObject<GameViewModel>(response.Content);

            if (obj.Demo)
            {
                obj.Type = "demo";
            }
            else
            {
                obj.Type = "full";
            }
            return View(obj);
        }
        public async Task<IActionResult> EditProcess(GameViewModel obItem)
        {
            string accessToken = await HttpContext.GetTokenAsync("access_token");
            var client = new RestClient("http://localhost:5000/api/Games/" + obItem.Id);
            var request = new RestRequest(Method.PUT);
            request.AddHeader("authorization", "Bearer " + accessToken);
            if(obItem.formFile != null)
            {
                var uploads = Path.Combine(_hostingEnvironment.WebRootPath, "imgs");
                FileUploadHelper objFile = new FileUploadHelper();
                string strFilePath = await objFile.SaveFileAsync(obItem.formFile, uploads);
                strFilePath = strFilePath
                 .Replace(_hostingEnvironment.WebRootPath, string.Empty)
                 .Replace("\\", "/");
                obItem.PhotoName = strFilePath.Split('/').Last();
            }
            request.AddJsonBody(new
            {
                Name = obItem.Name,
                Publisher = obItem.Publisher,
                Demo = obItem.Type == "demo",
                Price = obItem.Price,
                PhotoName = obItem.PhotoName
            }) ;
            IRestResponse response = client.Execute(request);
            return this.RedirectToAction("Index", "Home");
        }
    }
}
