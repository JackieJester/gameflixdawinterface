﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace GameFlix.Models
{
    public class UpgradeToPremiumViewModel
    {
        [Required]
        public string CardHolder_Name { get; set; }
        [Required]
        public int Card_Number { get; set; }

        public string Duration { get; set; }
        public List<SelectListItem> Durations { get; } = new List<SelectListItem>
         {
             new SelectListItem { Value = "30_days", Text = "30 days"},
             new SelectListItem { Value = "60_days", Text = "60 days"},
             new SelectListItem { Value = "90_days", Text = "90 days"}
         };
        [Required]
        public float Price { get; set; }

        public IFormFile formFile { get; set; }
    }
}
