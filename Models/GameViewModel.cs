﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace GameFlix.Models
{
    public class GameViewModel
    {
        public int Id { get; set; }
        public bool Demo { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public string Publisher { get; set; }
        [Required]
        public string Type { get; set; }

        public List<SelectListItem> GameTypes { get; } = new List<SelectListItem>
         {
             new SelectListItem { Value = "demo", Text = "Demo"},
             new SelectListItem { Value = "full", Text = "Full"}
         };
        [Required]
        public float Price { get; set; }

        public IFormFile formFile { get; set; }

        public string PhotoName { get; set; }

    }
}
