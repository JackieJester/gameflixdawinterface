﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Threading.Tasks;

namespace GameFlix
{
    public class Tools  
    {
        public static async Task<string> GetAccountTypeAsync(string accessToken)
        {
            
            var jwtToken = new JwtSecurityToken(accessToken);
            var permissions = jwtToken.Claims.Where(c => c.Type == "permissions").Select(c => c).ToList();
            if (permissions.FirstOrDefault(p => p.Value == "write:games") != null)
            {

               return "administrator";
            }
            else if (permissions.FirstOrDefault(p => p.Value == "write:reviews") != null)
            {

                return "premium";
            }
            else
            {
                return "free";
            }

        }
    }
}
